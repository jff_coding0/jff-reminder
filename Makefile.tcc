CC = tcc
CFLAGS = -Wall -O2
GTK3_CFLAGS = `pkg-config --cflags gtk+-3.0|sed 's/-pthread//'` -D_REENTRANT
GTK3_LIBS = `pkg-config --libs gtk+-3.0`
OBJS = main.o
SRC = main.c
OUTPUT = stub


all: $(OUTPUT)


$(OUTPUT): $(OBJS)
	$(CC) $(GTK3_LIBS) -o $(OUTPUT) $(OBJS)


$(OBJS): $(SRC)
	$(CC) $(GTK3_CFLAGS) $(CFLAGS) -c -o $@ $(SRC)


clean:
	rm -f $(OUTPUT) *.o
