CC = tcc
CFLAGS = -Wall -O2 -I./include/
GTK3_CFLAGS = `pkg-config --cflags gtk+-3.0`
GTK3_LIBS = `pkg-config --libs gtk+-3.0`
SQLITE3_LIBS = `pkg-config --libs sqlite3`
OBJS = main.o dbinit.o
OUTPUT = links_catalog


all: $(OUTPUT)


$(OUTPUT): $(OBJS)
	$(CC) $(SQLITE3_LIBS) $(GTK3_LIBS) -o $(OUTPUT) $^


%.o: src/%.c
	$(CC) $(CFLAGS) $(GTK3_CFLAGS) -c -o $@ $<


clean:
	rm -f $(OUTPUT) *.o
