#ifndef CONFIG_MAIN_H_
#define CONFIG_MAIN_H_


#define UNDER_CONSTRUCTION

#define LINKS_CATALOG_APPLICATION_ID  "org.alpha.links_catalog"

#define APPLICATION_DIRECTORY         ".local/links_catalog"

#define INIT_SCRIPT_NAME              "initdb.sql"

#define LINKS_DB_NAME                 "links.db"


#endif
