# Drop unneeded.
QT -= qt core gui


CONFIG += link_pkgconfig
PKGCONFIG += gtk+-3.0
PKGCONFIG += sqlite3


# ${project}/include/
INCLUDEPATH +=              \
    include/


# Sourcefiles.
SOURCES +=                  \
    src/main.c              \
    src/dbinit.c


# Project headers.
HEADERS +=                  \
    include/config.main.h \
    include/dbinit.h
