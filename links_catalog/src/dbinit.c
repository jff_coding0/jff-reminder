
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>

#include "dbinit.h"


char *
get_init_sql(const char *sql_fn) {
  struct stat l;
  if (stat(sql_fn, &l) == -1) {
    perror("[-] stat() failure");
    return NULL;
  }
  if ( !S_ISREG(l.st_mode) ) {

    fprintf(stderr,
            "[-] get_init_sql() failure: file %s is not a regular file\n",
            sql_fn);

    return NULL;

  }
  FILE *sql_file = fopen(sql_fn, "r");
  if (sql_file == NULL) {
    perror("[-] fopen() failure");
    return NULL;
  }

  char *sql_text = (char *)malloc(l.st_size + 1);
  sql_text[l.st_size] = NULCHR;

  if ( fread(sql_text, 1, l.st_size, sql_file) != (size_t)l.st_size ) {
    perror("[-] fread() failure");
    free(sql_text);
    sql_text = NULL;
  }

  fclose(sql_file);
  return sql_text;
}


sqlite3 *
init_db(const char *db_fn, const char *init_sql) {
  sqlite3 *db;

  int r = sqlite3_open_v2(db_fn,
                          &db,
                          SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE,
                          NULL);

  if (r != SQLITE_OK) {
    fprintf(stderr, "[-] sqlite3_open_v2() failure: %s\n", sqlite3_errmsg(db));
    return NULL;
  }

  fprintf(stderr, "[LOG] executing %s\n", init_sql);

  if ( sqlite3_exec(db, init_sql, NULL, NULL, NULL) != SQLITE_OK ) {
    fprintf(stderr, "[-] sqlite3_exec() failure: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    db = NULL;
  }

  return db;
}
