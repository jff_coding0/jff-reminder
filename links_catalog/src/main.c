
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <gtk/gtk.h>

#include "config.main.h"
#include "dbinit.h"


struct lc_application_info {
  GtkEntry *user_link_entry;
  GtkEntry *user_remark_entry;
  GtkListBox *links_container;
  sqlite3 *db;
};

struct lc_application_config {
  gchar *homedir_path;
  gchar application_dir_path[NAME_MAX];
  gchar db_location[NAME_MAX];
  gchar init_script_location[NAME_MAX];
};


static void
do_app(GtkApplication *app, struct lc_application_info *inf);


static int
prepare_application_directory(struct lc_application_config *app_config) {
  char localdir[NAME_MAX];
  g_snprintf(localdir, NAME_MAX, "%s/%s", app_config->homedir_path, ".local");
  struct stat l;

  if (stat(localdir, &l) == -1) {

    fprintf(stderr, "[-] cannot stat() %s dir: %s\n",
            localdir,
            strerror(errno));
    return 0;

  }
  if (!S_ISDIR(l.st_mode)) {
    fprintf(stderr, "[-] error: %s is not a directory\n", localdir);
    return 0;
  }

  if (stat(app_config->application_dir_path, &l) == -1) {
    if (errno == ENOENT) {

      fprintf(stderr,
              "[WARN] %s does not exist, trying to create it\n",
              app_config->application_dir_path);

      if (mkdir(app_config->application_dir_path, 0755) == -1) {
        perror("[-] mkdir() failure");
        return 0;
      } else {

        printf("[+] application directory %s is ready\n",
               app_config->application_dir_path);

      }

    } else {
      perror("[-] stat() failure for app directory");
    }
  } else {
    printf("[+] app directory ready\n");
  }

  return 1;
}


int
main(int argc, char **argv) {
  static const char *application_dir_ = APPLICATION_DIRECTORY;
  static const char *links_db_name_ = LINKS_DB_NAME;
  static struct lc_application_config appconfig_;

  appconfig_.homedir_path = getenv("HOME");

  if (appconfig_.homedir_path == NULL) {
    perror("[-] failed to locate home directory");
    exit(EXIT_FAILURE);
  }

  g_snprintf(appconfig_.application_dir_path,
             NAME_MAX,
             "%s/%s",
             appconfig_.homedir_path,
             application_dir_);

  prepare_application_directory(&appconfig_);

  g_snprintf(appconfig_.db_location,
             NAME_MAX,
             "%s/%s",
             appconfig_.application_dir_path,
             links_db_name_);

  g_print("[LOG] Should locate db at %s\n", appconfig_.db_location);

  if (argc > 1 && strcmp(argv[1], "--init") == 0) {
    static const char *dbinit_script_name_ = INIT_SCRIPT_NAME;

    g_snprintf(appconfig_.init_script_location,
               NAME_MAX,
               "%s/%s",
               appconfig_.application_dir_path,
               dbinit_script_name_);

    char *init_sql = get_init_sql(appconfig_.init_script_location);
    if (init_sql == NULL) {
      exit(EXIT_FAILURE);
    }
    sqlite3 *db = init_db(appconfig_.db_location, init_sql);
    if (db == NULL) {
      exit(EXIT_FAILURE);
    }
    sqlite3_close_v2(db);
    puts("Initialization complete\n");
    exit(EXIT_SUCCESS);
  }

#ifdef UNDER_CONSTRUCTION
  printf("[WARN] Tool is under construction now\n");
  struct stat l;
  if (stat(appconfig_.db_location, &l) == -1) {

    fprintf(stderr,
            "[WARN] Database wasn't initialized yet. Please run %s --init.",
            argv[0]);

  }
#endif
  sqlite3 *links_db;

  int r = sqlite3_open_v2(appconfig_.db_location,
                          &links_db,
                          SQLITE_OPEN_READWRITE,
                          NULL);

  if (r != SQLITE_OK) {

    fprintf(stderr,
            "[-] sqlite3_open_v2() failure: %s\n",
            sqlite3_errmsg(links_db));

    exit(EXIT_FAILURE);

  }

  GtkApplication *app = gtk_application_new(LINKS_CATALOG_APPLICATION_ID,
                                            G_APPLICATION_FLAGS_NONE);

  struct lc_application_info inf;
  inf.db = links_db;
  g_signal_connect(app, "activate", G_CALLBACK(do_app), &inf);
  int return_code = g_application_run(G_APPLICATION(app), argc, argv);
  sqlite3_close_v2(links_db);
  return return_code;
}


static int
sql_select_callback(void *ui_container, int count, char **vals, char **names) {

  static const char href_format_str_[] = "<a href=\"%s\">%s</a>";
  GtkListBox *lb = GTK_LIST_BOX( ui_container );
  GtkLabel *l = GTK_LABEL( gtk_label_new(NULL) );
  char *markupbuf = NULL;

  g_print("Adding => %s:\t%s\n", vals[0], vals[1]);

  /* Exclude double "%s" template. */
  unsigned long link_len = strlen(vals[0]) + strlen(vals[1]) +
      sizeof(href_format_str_) - 4;

  markupbuf = (char *)malloc(link_len);

  g_snprintf(markupbuf,
             link_len,
             href_format_str_,
             vals[0], vals[1]);

  gtk_label_set_markup(l, markupbuf);

  gtk_list_box_insert(lb, GTK_WIDGET(l), -1);
  free(markupbuf);

  return 0;

  (void)count, (void)names;

}


static void
insert_link_clicked_callback(GtkButton *b, struct lc_application_info *info) {
  (void)b;
  sqlite3_stmt *prepared;

  if (sqlite3_prepare_v2(info->db,
                         "INSERT INTO links (link, remark) VALUES (?, ?);",
                         -1,
                         &prepared,
                         NULL) != SQLITE_OK) {

    g_printerr("[-] sqlite3_prepare_v2() failure(): %s\n",
               sqlite3_errmsg(info->db));

    return;

  }

  GtkEntry *inputs[] = { info->user_link_entry, info->user_remark_entry };
  for (int i = 1; i <= 2; ++ i) {
    if (sqlite3_bind_text(prepared,
                      i,
                      gtk_entry_get_text(inputs[i - 1]),
                      -1,
                      SQLITE_STATIC) != SQLITE_OK) {

      g_printerr("[-] sqlite3_bind_text(%i) failure(): %s\n",
                 i, sqlite3_errmsg(info->db));

      sqlite3_finalize(prepared);
      return;

    }
  }

  if (sqlite3_step(prepared) != SQLITE_DONE) {

    g_printerr("[-] sqlite3_step() failure(): %s\n",
               sqlite3_errmsg(info->db));

  } else {
    const gchar *usertext = gtk_entry_get_text(info->user_link_entry);
    size_t s_len = strlen(usertext);
    char *linktext = (char *)malloc(s_len + 64);

    g_snprintf(linktext, s_len + 64, "<a href=\"%s\">%s</a>",
               usertext, gtk_entry_get_text(info->user_remark_entry));

    g_print("UI: adding text \"%s\"\n", linktext);
    GtkLabel *l = GTK_LABEL( gtk_label_new(NULL) );
    gtk_label_set_markup(l, linktext);
    gtk_list_box_insert(info->links_container, GTK_WIDGET(l), -1);

    gtk_widget_show(GTK_WIDGET(l));
    free(linktext);
    gtk_entry_set_text(info->user_link_entry, "");
    gtk_entry_set_text(info->user_remark_entry, "");
  }
  sqlite3_finalize(prepared);
}


static void
quit_button_clicked(GtkWidget *b, GtkWindow *main_window) {
  (void)b;
  g_signal_emit_by_name(main_window, "destroy");
}


static void
do_app(GtkApplication *app, struct lc_application_info *inf) {
  GtkWindow *main_window = GTK_WINDOW( gtk_application_window_new(app) );
  gtk_window_set_default_size(main_window, 400, 600);

  GtkScrolledWindow *scroller =
      GTK_SCROLLED_WINDOW( gtk_scrolled_window_new(NULL, NULL) );

  gtk_widget_set_vexpand(GTK_WIDGET(scroller), gtk_true());

  inf->links_container = GTK_LIST_BOX( gtk_list_box_new() );
  gtk_list_box_set_selection_mode(inf->links_container, GTK_SELECTION_NONE);

  sqlite3_exec(inf->db,
               "SELECT link, remark FROM links;",
               sql_select_callback,
               inf->links_container,
               NULL);

  GtkBox *widgets_box = GTK_BOX( gtk_box_new(GTK_ORIENTATION_VERTICAL, 2) );

  GtkBox *insert_controls_box =
      GTK_BOX( gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2) );

  GtkButton *insert_link_button =
      GTK_BUTTON( gtk_button_new_with_label("Add link") );

  g_signal_connect(insert_link_button,
                   "clicked",
                   G_CALLBACK(insert_link_clicked_callback),
                   inf);

  gtk_box_set_homogeneous(insert_controls_box, gtk_true());

  inf->user_link_entry = GTK_ENTRY( gtk_entry_new() );

  gtk_entry_set_placeholder_text(inf->user_link_entry,
                                 "Place your new link here");

  inf->user_remark_entry = GTK_ENTRY( gtk_entry_new() );
  gtk_entry_set_placeholder_text(inf->user_remark_entry, "Description of link");

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                    GTK_WIDGET(inf->user_link_entry));

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                    GTK_WIDGET(inf->user_remark_entry));

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                   GTK_WIDGET(insert_link_button));

  gtk_container_add(GTK_CONTAINER(scroller), GTK_WIDGET(inf->links_container));

  gtk_container_add(GTK_CONTAINER(widgets_box),
                    GTK_WIDGET(insert_controls_box));

  gtk_container_add(GTK_CONTAINER(widgets_box), GTK_WIDGET(scroller));

  gtk_container_add(GTK_CONTAINER(main_window), GTK_WIDGET(widgets_box));

  GtkButton *quit_button =  GTK_BUTTON( gtk_button_new_with_label("Quit") );

  g_signal_connect(quit_button,
                   "clicked",
                   G_CALLBACK(quit_button_clicked),
                   main_window);

  gtk_container_add(GTK_CONTAINER(widgets_box), GTK_WIDGET(quit_button));

  gtk_widget_show_all(GTK_WIDGET(main_window));
}
