
#include <ctype.h>
#include <stdlib.h>

#include <gtk/gtk.h>


#define ELEM_COUNT(x) sizeof(x) / sizeof(x[0])

#define SHARPED_CLR_LEN 32


/* Application specific data. */
static const char application_id_[] = "org.alpha.anycolour";
static const char main_window_title_[] = "Any colour you like...";
static const unsigned int main_window_def_width_ = 400;
static const unsigned int main_window_def_height_ = 600;
static const char button_fl_text_[] = "I Feel Lucky";


/* Gtk widget signal names used in app. */
static const char sig_drawing_area_draw_[] = "draw";
static const char sig_button_clicked_[] = "clicked";
static const char sig_scale_value_changed_[] = "value-changed";
static const char sig_application_activate_[] = "activate";


struct color_data {
  double red_part;
  double green_part;
  double blue_part;
};

struct color_control {
  struct color_data *colors;
  char hexed_color[SHARPED_CLR_LEN];
  GtkEntry *color_hint;
  GtkScale *red_slider;
  GtkScale *green_slider;
  GtkScale *blue_slider;
  GtkDrawingArea *canvas;
};


double
get_colour_part(unsigned int value) {
  if (value >= 255) {
    return 1.0;
  }
  return (value / 2.55) / 100.0;
}


void
hexify(struct color_control *ctrl) {
  g_snprintf(ctrl->hexed_color, SHARPED_CLR_LEN, "#%02X%02X%02X",
             (unsigned int)(ctrl->colors->red_part * 255),
             (unsigned int)(ctrl->colors->green_part * 255),
             (unsigned int)(ctrl->colors->blue_part * 255));
}


static gboolean
draw_callback(GtkWidget *widget, cairo_t *cr,
              const struct color_data *clr_data) {

  static const unsigned int x_offset = 40;
  static const unsigned int y_offset = 60;

  cairo_rectangle(cr,
                  x_offset,
                  y_offset,
                  gtk_widget_get_allocated_width(widget) - x_offset * 2,
                  gtk_widget_get_allocated_height(widget) - y_offset * 2);

  cairo_set_source_rgb(cr,
                       clr_data->red_part,
                       clr_data->green_part,
                       clr_data->blue_part);

  cairo_fill(cr);
  gtk_widget_queue_draw(widget);
  return FALSE;

}


static void
channel_value_changed_callback(GtkScale *sender, struct color_control *ctrl) {

  double *part_select = NULL;
  if (sender == ctrl->red_slider) {
    part_select = &ctrl->colors->red_part;
  } else if (sender == ctrl->green_slider) {
    part_select = &ctrl->colors->green_part;
  } else if (sender == ctrl->blue_slider) {
    part_select = &ctrl->colors->blue_part;
  } else {
    g_error("Oops... Bad slider pointer\n");
  }

  if (part_select != NULL) {

    *part_select =
        get_colour_part( gtk_range_get_value(GTK_RANGE(sender)) );

    hexify(ctrl);

    gtk_entry_set_text(ctrl->color_hint, ctrl->hexed_color);

  }

}


static void
lucky_button_clicked_callback(GtkButton *sender, struct color_control *ctrl) {
  (void)sender;
  GRand *grand_seed = g_rand_new();

  gtk_range_set_value(GTK_RANGE(ctrl->red_slider),
                      g_rand_int_range(grand_seed, 0, 255));

  gtk_range_set_value(GTK_RANGE(ctrl->green_slider),
                      g_rand_int_range(grand_seed, 0, 255));

  gtk_range_set_value(GTK_RANGE(ctrl->blue_slider),
                      g_rand_int_range(grand_seed, 0, 255));
}


static GtkScale *
setup_slider(double cpart) {

  GtkScale *slider =
      GTK_SCALE( gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL,
                                          0.0, 255.0, 1.0) );

  gtk_range_set_value(GTK_RANGE(slider), (unsigned int)(cpart * 255));

  return slider;

}


static void
do_app(GtkApplication *app, struct color_control *control) {

  hexify(control);

  control->color_hint = GTK_ENTRY( gtk_entry_new() );

  GValue b_false = G_VALUE_INIT;
  g_value_init(&b_false, G_TYPE_BOOLEAN);
  g_value_set_boolean(&b_false, FALSE);
  g_object_set_property(G_OBJECT(control->color_hint), "editable", &b_false);
  g_object_set_property(G_OBJECT(control->color_hint), "can-focus", &b_false);
  gtk_entry_set_alignment(control->color_hint, 0.5);
  gtk_entry_set_text(control->color_hint, control->hexed_color);

  control->canvas = GTK_DRAWING_AREA( gtk_drawing_area_new() );
  gtk_widget_set_vexpand(GTK_WIDGET(control->canvas), gtk_true());

  g_signal_connect(control->canvas,
                   sig_drawing_area_draw_,
                   G_CALLBACK(draw_callback),
                   (gpointer)control->colors);

  GtkBox *vbox = GTK_BOX( gtk_box_new(GTK_ORIENTATION_VERTICAL, 4) );

  GtkButton *lucky_button =
      GTK_BUTTON( gtk_button_new_with_label(button_fl_text_) );

  g_signal_connect(lucky_button,
                   sig_button_clicked_,
                   G_CALLBACK(lucky_button_clicked_callback),
                   (gpointer)control);

  control->red_slider = setup_slider(control->colors->red_part);
  control->green_slider = setup_slider(control->colors->green_part);
  control->blue_slider = setup_slider(control->colors->blue_part);

  GtkScale *sliders[] =
    { control->red_slider, control->green_slider, control->blue_slider };

  for (size_t i = 0; i < ELEM_COUNT(sliders); ++ i) {

    g_signal_connect(sliders[i],
                     sig_scale_value_changed_,
                     G_CALLBACK(channel_value_changed_callback),
                     (gpointer)control);

  }

  GtkWindow *main_window = GTK_WINDOW( gtk_application_window_new(app) );
  gtk_window_set_title(main_window, main_window_title_);

  gtk_window_set_default_size(main_window,
                              main_window_def_width_,
                              main_window_def_height_);

  gpointer vbox_widgets[] = {
    control->color_hint,
    lucky_button,
    control->red_slider,
    control->green_slider,
    control->blue_slider,
    control->canvas
  };

  for (size_t i = 0; i < ELEM_COUNT(vbox_widgets); ++ i) {
    gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(vbox_widgets[i]));
  }

  gtk_container_add(GTK_CONTAINER(main_window), GTK_WIDGET(vbox));

  gtk_widget_show_all(GTK_WIDGET(main_window));
}


void
usage(const char *exename) {
  g_print("Usage:\t%s <red> <green> <blue>, from 0 to 255\n"
          "\t%s <#colorspec>\n",
          exename, exename);
}


inline unsigned int
hextobyte(char symp) {
  return (symp <= '9' ? symp - '0' : tolower((unsigned char)symp) - 'a' + 10);
}


void
convert_spec(const char *spec, struct color_data *target) {
  if (spec[0] != '#' || strnlen(spec, 9) != 7) {
    return;
  }
  const char *colorptr = spec + 1;
  unsigned int rgb[3] = {0,};
  for (size_t i = 0; i < 3; ++ i) {
    rgb[i] = hextobyte(colorptr[i * 2]) * 16 +
        hextobyte(colorptr[i * 2 + 1]);
  }
  target->red_part = get_colour_part(rgb[0]);
  target->green_part = get_colour_part(rgb[1]);
  target->blue_part = get_colour_part(rgb[2]);
}


int
main(int argc, char **argv) {
  struct color_data clrdata = {0,};
  if (argc > 3) {
    clrdata.red_part = get_colour_part(atoi(argv[1]));
    clrdata.green_part = get_colour_part(atoi(argv[2]));
    clrdata.blue_part = get_colour_part(atoi(argv[3]));
  } else if (argc > 1 && argv[1][0] == '#') {
    convert_spec(argv[1], &clrdata);
  } else {
    usage(argv[0]);
    return 0;
  }
  struct color_control ctrl;
  ctrl.colors = &clrdata;

  GtkApplication *app =
      gtk_application_new(application_id_, G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, sig_application_activate_, G_CALLBACK(do_app), &ctrl);

  int exit_code = g_application_run(G_APPLICATION(app), 1, argv);

  g_print("%s\n", ctrl.hexed_color);

  return exit_code;
}
